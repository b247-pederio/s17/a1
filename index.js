/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	first function here:
	function promtUser(fullName, age, location) {
		fullName = prompt('Please Enter Your Full Name');
		age = prompt('Please Enter Your Age');
		location = prompt('Please Enter Your Location');
		alert('Thank you for your inputs!');
		console.log('Hello, ' + fullName)
		console.log('You are ' + age + ' years old.')
		console.log('You live in ' + location)
	};
	promtUser();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

	//second function here:
	function favArtists(artOne, artTwo, artThree, artFour, artFive) {
		artOne = '1. Mariah Carey'
		artTwo = '2. Adele'
		artThree = '3. Eminem'
		artFour = '4. 50 Cent'
		artFive = '5. Snoop Dogg'
		console.log(artOne)
		console.log(artTwo)
		console.log(artThree)
		console.log(artFour)
		console.log(artFive)
	}
	favArtists()

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//third function here:
	function favMovies(movie1, movie2, movie3, movie4, movie5) {
		movie1 = '1. Apocalypto'
		console.log(movie1)
		console.log('Rotten Tomatoes Rating: 65%')
		movie2 = '2. The Gentleman'
		console.log(movie2)
		console.log('Rotten Tomatoes Rating: 75%')
		movie3 = '3. Spirited Away'
		console.log(movie3)
		console.log('Rotten Tomatoes Rating: 97%')
		movie4 = '4. Gladiator'
		console.log(movie4)
		console.log('Rotten Tomatoes Rating: 79%')
		movie5 = '5. 300'
		console.log(movie5)
		console.log('Rotten Tomatoes Rating: 61%')
	}
	favMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();