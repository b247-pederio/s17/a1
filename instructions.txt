Activity Instruction:

1. In the s17 folder, create an activity folder, an index.html file inside of it and link the index.js file.
2. Create an index.js file and console log the message Hello World to ensure that the script file is properly associated with the html file.
3. Copy the activity code from your Boodle Notes. Paste the activity code from your Boodle Notes to your index.js file.
4. Create a function which is able to prompt the user to provide their full name, age, and location. 
    - use prompt() and store the returned value into function scoped variables within the function. 
    - show an alert to thank the user for their input.
    - display the user's inputs in messages in the console.
    - invoke the function to display the user’s information in the console.
    - follow the naming conventions for functions.
5. Create a function which is able to print/display your top 5 favourite bands/musical artists. 
    - invoke the function to display your information in the console.
    - follow the naming conventions for functions.
6. Create a function which is able to print/display your top 5 favourite movies of all time and show Rotten Tomatoes rating. 
    - look up the Rotten Tomatoes rating of your favourite movies and display it along with the title of your favourite movie.
    - invoke the function to display your information in the console.
    - follow the naming conventions for functions.
7. Debugging Practice - Debug the given codes and functions to avoid errors.
    - check the variable names.
    - check the variable scope.
    - check function invocation/declaration.
    - comment out unusable codes.
8. Create a git repository named s17.
9. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
10. Add the link in Boodle.

Activity - How you will be evaluated
    a. No errors should be logged in the console.
    b. prompt() is used to gather information.
    c. alert() is used to show information.
    d. All values must be properly logged in the console.
    e. All variables are named appropriately and defines the value it contains.
    f. All functions are named appropriately and follows naming conventions.